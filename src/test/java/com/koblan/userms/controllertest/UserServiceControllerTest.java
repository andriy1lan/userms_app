package com.koblan.userms.controllertest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.service.RoleService;
import com.koblan.userms.service.UserService;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;

public class UserServiceControllerTest extends AbstractTest {
	
	   @Autowired
	   RoleService roleService;
	   @Autowired
	   RoleRepository roleRepository;
	   @Autowired
	   UserService userService;
	   @Autowired
	   UserRepository userRepository;
	   
	   @Before
	   public void setUp() {
		   super.setUp();
	   }
	   
	   @Before
	   public void open() {

	   }
	   
	   @After
	    public void close() {
		   
	    }
	   
	   @Test
	   public void getUsersList() throws Exception {
	      String uri = "/users";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
	         .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      User[] userlist = super.mapFromJson(content, User[].class);
	      assertTrue("List of User retrived",userlist.length > 0);
	   }
	   
	   //@Ignore
	   @Test
	   public void createUser() throws Exception {
	      String uri = "/users";
	      User user = new User();
	      user.setName("Ivan");
	      user.setUsername("ivann");
	      user.setEmail("ivann@meta.ua");
	      user.setPassword("333");
	      Set<Role> roles=new HashSet<Role>();
	      roles.add(new Role("ROLE_GUEST"));
	      user.setRoles(roles);
	      String inputJson = super.mapToJson(user);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(201, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      //assertEquals(content, "User is created successfully");
	      User user1=super.mapFromJson(content, User.class);
	      assertEquals("name equals",user.getName(), user1.getName());
	      assertEquals("username equals",user.getUsername(), user1.getUsername());
	      assertEquals("email equals",user.getEmail(), user1.getEmail());
	      
	   }
	   
	   //@Ignore
	   @Test
	   public void updateUser() throws Exception {
	      String uri = "/users/1";
	      User user = new User();
	      user.setName("Igor");
	      user.setUsername("Artii");
	      user.setEmail("ArtemenIgor@gmail.com");
	      user.setPassword("333");
	      Set<Role> roles=new HashSet<Role>();
	      Role r=new Role("ROLE_ADMIN");
	      r.setId(roleRepository.findByRoleIgnoreCase("ROLE_ADMIN").getId());
	      System.out.println(roleRepository.findByRoleIgnoreCase("ROLE_ADMIN").getId());
	      roles.add(r);
	      user.setRoles(roles);
	      String inputJson = super.mapToJson(user);
	      System.out.println(inputJson);
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri)
	         .contentType(MediaType.APPLICATION_JSON_VALUE)
	         .content(inputJson)).andReturn();
	      
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals(200, status);
	      String content = mvcResult.getResponse().getContentAsString();
	      User user1=super.mapFromJson(content, User.class);
	      assertEquals("name equals",user.getName(), user1.getName());
	      assertEquals("username equals",user.getUsername(), user1.getUsername());
	      assertEquals("email equals",user.getEmail(), user1.getEmail());
	      //assertEquals("username equals",user.getPassword(), user1.getPassword()); password encoded after update - so changed - not equal
	      assertEquals("user received admin role",user.getRoles().iterator().next().getRole().toString(), user1.getRoles().iterator().next().getRole().toString().toString());
	   }
	   
	   @Ignore
	   @Test
	   public void deleteUser() throws Exception {
	      String uri = "/users/2";
	      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri)).andReturn();
	      int status = mvcResult.getResponse().getStatus();
	      assertEquals("Deleted", 200, status);
	   }
}
