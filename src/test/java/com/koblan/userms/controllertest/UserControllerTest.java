package com.koblan.userms.controllertest;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.koblan.userms.model.*;
import com.koblan.userms.repository.UserRepository;
import com.koblan.userms.service.*;
import com.koblan.userms.controller.UserController;

@ContextConfiguration(classes = {TestConfig.class})
@RunWith(SpringRunner.class)
//@ActiveProfiles("test")
//@AutoConfigureMockMvc(secure = false)
@WebMvcTest(value = UserController.class)  //, secure = false
public class UserControllerTest {
	
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserService userService; 
	
	User mockuser;
	
	@Before
	public void setUp() {
	mockuser=new User("Oxana","oxak","oxana@ukr.net","123");
	mockuser.setId(1L);
	Role role=new Role("ROLE_GUEST");
	role.setId(8L); // Id of Guest Role
	Set<Role> roles=new HashSet<Role>(); roles.add(role);
	mockuser.setRoles(roles);
	}
    

	@Test
	public void retrieveUser() throws Exception {
		
		Mockito.when(
				userService.getUser(Mockito.anyLong())).thenReturn(mockuser);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/users/1").accept(
				MediaType.APPLICATION_JSON).characterEncoding("utf-8");
		System.out.println("AAA"+ requestBuilder.toString() +"BB");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println("AA"+ result.getResponse().getContentAsString() +"BB");
		AbstractTest converter=new AbstractTest();
		String expected = converter.mapToJson(mockuser);
		System.out.println(expected);
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
		assertEquals(expected, result.getResponse()
				.getContentAsString());
	  }
	
	
	@Test
	public void createUser() throws Exception {
		mockuser=new User("Roman","romaa","romann@yahoo.com","111");
		mockuser.setId(23L);
		Role role=new Role("ROLE_GUEST");
		role.setId(8L); // Id of Guest Role
		Set<Role> roles=new HashSet<Role>(); roles.add(role);
		mockuser.setRoles(roles);

		// userService.createUser to respond back with mockuser
		Mockito.when(
				userService.createUser(Mockito.any())).thenReturn(mockuser);

		// Send user as body to /users
		AbstractTest converter=new AbstractTest();
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/users")
				.accept(MediaType.APPLICATION_JSON).content(converter.mapToJson(mockuser))
				.contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
		

	}
      
}
