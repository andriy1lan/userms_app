package com.koblan.userms.controllertest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
//import org.springframework.actuate.autoconfigure.ManagementSecurityAutoConfiguration;

import com.koblan.userms.repository.UserRepository;
import com.koblan.userms.service.UserService;

@Configuration
//@ComponentScan({"com.koblan.userms.repository"})
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
public class TestConfig {

    @Bean
    public UserRepository userRepository1(){
    	
        return new UserService().getUserRepository();
    } 
}