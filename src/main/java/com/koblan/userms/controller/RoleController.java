package com.koblan.userms.controller;

import com.koblan.userms.service.RoleService;
import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.exceptions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.apache.log4j.Logger;



@RestController
public class RoleController {
	
	private static Logger logger = LogManager.getLogger(RoleController.class.getName());
	
	 @Autowired
	 RoleService roleService;
	 
	 @GetMapping(value = "/roles")
	 public ResponseEntity<List<Role>> getAllRoles() throws NoSuchRoleException {
	        List<Role> roleList = roleService.getAllRoles();
	        //System.out.println(org.hibernate.Version.getVersionString());
	        logger.info("get ALL ROLEEEEEEEEEEEEEEEES");
	        //logger.info( System.getProperty("log4j.configurationFile"));
	        return new ResponseEntity<>(roleList, HttpStatus.OK);
	        
	    }
	 
	 @PostMapping("/roles")
     public ResponseEntity<Role> addRole(@RequestBody Role role) {
			 Role nrole=roleService.createRole(role);
			 return new ResponseEntity<>(nrole, HttpStatus.CREATED);
		}
	 
	 @PutMapping(value = "/roles/{id}")
	    public  ResponseEntity<Role> updateRole(@RequestBody Role uRole, @PathVariable Long id) throws NoSuchRoleException {
	        roleService.updateRole(uRole, id);
	        Role role = roleService.getRole(id);
	        return new ResponseEntity<>(role, HttpStatus.OK);
	    }
	 
	 @DeleteMapping("/roles/{id}")
	 public ResponseEntity deleteRole(@PathVariable Long id) throws NoSuchRoleException, ExistsUsersForRoleException {
			roleService.deleteRole(id);
			return new ResponseEntity(HttpStatus.OK);
		}
	 
}