package com.koblan.userms.controller;


import com.koblan.userms.service.UserService;
import com.koblan.userms.service.UserService1;
import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
//import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.exceptions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
public class UserController1 {
	
	private static Logger logger = LogManager.getLogger();
	 
	 //@Autowired
	 UserService1 userService=new UserService1();
	 
	 @PreAuthorize("hasRole('GUEST') or hasRole('ADMIN')")
	 @GetMapping(value = "/users1")
	 public ResponseEntity<List<User>> getAllUsers() throws NoSuchUserException {
	        List<User> userList = userService.getAllUsers();
	        return new ResponseEntity<>(userList, HttpStatus.OK);
	    }
	 
	 //@PreAuthorize("hasRole('ADMIN')")
	 @GetMapping(value = "/users1/{id}")
	 public ResponseEntity<User> getUser(@PathVariable Long id) throws NoSuchUserException {
	        User user=userService.getUser(id);
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @PostMapping(value="/users1")
     public ResponseEntity<User> addUser(@RequestBody User user) {
			 User nuser=userService.createUser(user);
			 return new ResponseEntity<>(nuser, HttpStatus.CREATED);
		}
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @PostMapping("/users1_a")
     public ResponseEntity<User> addAdmin(@RequestBody User user) {
			 User nuser=userService.createUserWithAdminRole(user);
			 return new ResponseEntity<>(nuser, HttpStatus.CREATED);
		}
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @PutMapping(value = "/users1/{id}")
	    public  ResponseEntity<User> updateUser(@RequestBody User nuser, @PathVariable Long id) throws NoSuchUserException {
	        userService.updateUser(nuser, id);
	        User user = userService.getUser(id);
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @DeleteMapping("/users1/{id}")
	 public ResponseEntity deleteUser(@PathVariable Long id) throws NoSuchUserException {
			userService.deleteUser(id);
			return new ResponseEntity(HttpStatus.OK);
		}
	 
}