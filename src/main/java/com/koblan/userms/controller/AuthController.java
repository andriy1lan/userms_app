package com.koblan.userms.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.koblan.userms.exceptions.NoSuchRoleException;
import com.koblan.userms.model.Message;
import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import com.koblan.userms.security.JwtResponse;
import com.koblan.userms.security.LoginData;
import com.koblan.userms.security.SignupData;
import com.koblan.userms.security.jwt.JwtProvider;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;



@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AuthController {
	
	private static final Logger logger = LogManager.getLogger();
	
	@Autowired
    AuthenticationManager authenticationManager;
 
    @Autowired
    UserRepository userRepository;
 
    @Autowired
    RoleRepository roleRepository;
 
    @Autowired
    PasswordEncoder encoder;
 
    @Autowired
    JwtProvider jwtProvider;
    
    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginData loginRequest) {	
    	logger.info(loginRequest);
    	
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        
        if (authentication==null) throw new BadCredentialsException("Login failed - Check username/password");
        
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        logger.warn(userDetails);
        logger.info(jwt);
        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }
    
    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupData signUpRequest) {
    	//public ResponseEntity<String> registerUser
    	logger.info(signUpRequest);
    	
    	if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<String>("Username already exists",
                    HttpStatus.BAD_REQUEST);
        }
       
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<String>("Email already exists!",
                    HttpStatus.BAD_REQUEST);
        }
 
        // Creating User Account with Guest role
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));
 
        Set<String> stringRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();
        logger.error(stringRoles);
        
	    Role guestRole = roleRepository.findByRoleIgnoreCase("ROLE_GUEST");
	    if (guestRole!=null) roles.add(guestRole);
	    else throw new NoSuchRoleException();       			
        user.setRoles(roles);
        
        userRepository.save(user);
        //return ResponseEntity.ok().body("User registered successfully!");
        return new ResponseEntity<>(new Message("User registered successfully!"), HttpStatus.OK);
    }
    
    @PostMapping("/signup_a")
    public ResponseEntity<?> registerUser_a(@Valid @RequestBody SignupData signUpRequest) {
    	logger.info(signUpRequest);
    	
    	if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<String>("Username already exists",
                    HttpStatus.BAD_REQUEST);
        }
       
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<String>("Email already exists!",
                    HttpStatus.BAD_REQUEST);
        }
 
        // Creating User with Admin and Guest Roles
        User user = new User(signUpRequest.getName(), signUpRequest.getUsername(),
                signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));
 
        //Set<String> stringRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();
	    Role adminRole = roleRepository.findByRoleIgnoreCase("ROLE_ADMIN");
	    if (adminRole!=null) roles.add(adminRole);
	    else throw new NoSuchRoleException();
	    Role guestRole = roleRepository.findByRoleIgnoreCase("ROLE_GUEST");
	    if (guestRole!=null) roles.add(guestRole);
	    else throw new NoSuchRoleException();
	    roles.add(guestRole);       
	    
        user.setRoles(roles);
        userRepository.save(user);
        //return ResponseEntity.ok().body("User with Admin role registered successfully!");
        return new ResponseEntity<>(new Message("User with Admin role registered successfully!"), HttpStatus.OK);
    }

}
