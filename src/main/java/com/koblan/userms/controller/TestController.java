package com.koblan.userms.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
		
		@GetMapping("/u_a")
		@PreAuthorize("hasRole('GUEST') or hasRole('ADMIN')")
		public ResponseEntity<String> userAccess() {
			return new ResponseEntity<String>("Admin and Guest allowed!",HttpStatus.OK);
		}
		
		@GetMapping("/u1")
		@PreAuthorize("hasRole('GUEST')")
		public String projectManagementAccess() {
			return "Guest Allowed";
		}
		
		@GetMapping("/a1")
		@PreAuthorize("hasRole('ADMIN')")
		public String adminAccess() {
			return "Admin Allowed";
		}
	}
