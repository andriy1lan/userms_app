package com.koblan.userms.controller;

import com.koblan.userms.model.Message;
import com.koblan.userms.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.MalformedJwtException; 
import io.jsonwebtoken.ExpiredJwtException;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(NoSuchRoleException.class)
    ResponseEntity<Message> showNoSuchRoleException(){
        return new ResponseEntity<Message>(new Message("Such role not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(NoSuchUserException.class)
    ResponseEntity<Message> showNoSuchUserException(){
        return new ResponseEntity<Message>(new Message("Such user not found"), HttpStatus.NOT_FOUND);
    }
	
	@ExceptionHandler(ExistsUsersForRoleException.class)
    ResponseEntity<Message> showExistsUsersForRoleException(){
        return new ResponseEntity<Message>(new Message("There are users for this role, cannot remove role"), HttpStatus.CONFLICT);
    }
	
	@ExceptionHandler(SignatureException.class)
    ResponseEntity<Message> showNotCorrectJWTSignature(){
        return new ResponseEntity<Message>(new Message("JWT signature does not match locally computed signature and should noy be trusted"), HttpStatus.CONFLICT);
    }
	
	@ExceptionHandler(MalformedJwtException.class)
    ResponseEntity<Message> showMalformedJWTString(){
        return new ResponseEntity<Message>(new Message("JWT strings must contain exactly 2 period characters/Unable to read JSON value"), HttpStatus.CONFLICT);
    }
	
	@ExceptionHandler(ExpiredJwtException.class)
    ResponseEntity<Message> expiredJWTToken(){
        return new ResponseEntity<Message>(new Message("Expired JWT token"), HttpStatus.CONFLICT); }
        
    @ExceptionHandler(AuthenticationException.class)
    ResponseEntity<Message> sendFailedLogin(){
        return new ResponseEntity<Message>(new Message("Login failed - Check username/password"), HttpStatus.UNAUTHORIZED);
    }
}
