package com.koblan.userms.controller;

import com.koblan.userms.service.UserService;
import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.UserRepository;
//import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.exceptions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
	
	private static Logger logger = LogManager.getLogger();
	 
	 @Autowired
	 UserService userService;
	 
	 @Autowired
	 UserRepository userRepository;
	 
	 @GetMapping(value = "/users")
	 public ResponseEntity<List<User>> getAllUsers() throws NoSuchUserException {
	        List<User> userList = userService.getAllUsers();
	        return new ResponseEntity<>(userList, HttpStatus.OK);
	    }
	 
	 @GetMapping(value = "/users/{id}")
	 public ResponseEntity<User> getUser(@PathVariable Long id) throws NoSuchUserException {
	        User user=userService.getUser(id);
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
	 
	 @GetMapping(value = "/users/name/{username}")
	 public ResponseEntity<User> getUser(@PathVariable String username) throws NoSuchUserException {
	        User user=userRepository.findByUsernameIgnoreCase(username);
	        if (user == null) throw new NoSuchUserException();
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
	 
	 @PostMapping(value="/users")
     public ResponseEntity<User> addUser(@RequestBody User user) {
			 User nuser=userService.createUser(user);
			 return new ResponseEntity<>(nuser, HttpStatus.CREATED);
		}
	 
	 @PostMapping("/users_a")
     public ResponseEntity<User> addAdmin(@RequestBody User user) {
			 User nuser=userService.createUserWithAdminRole(user);
			 return new ResponseEntity<>(nuser, HttpStatus.CREATED);
		}
	 
	 @PutMapping(value = "/users/{id}")
	    public  ResponseEntity<User> updateUser(@RequestBody User nuser, @PathVariable Long id) throws NoSuchUserException {
	        userService.updateUser(nuser, id);
	        User user = userService.getUser(id);
	        return new ResponseEntity<>(user, HttpStatus.OK);
	    }
	 
	 @DeleteMapping("/users/{id}")
	 public ResponseEntity deleteUser(@PathVariable Long id) throws NoSuchUserException {
			userService.deleteUser(id);
			return new ResponseEntity(HttpStatus.OK);
		}
	 
}