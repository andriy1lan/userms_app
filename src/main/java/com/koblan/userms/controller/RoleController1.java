package com.koblan.userms.controller;

//import com.koblan.userms.service.RoleService;
import com.koblan.userms.service.RoleService1;
import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
//import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.exceptions.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



@RestController
public class RoleController1 {
	
	private static Logger logger = LogManager.getLogger();
	
	 RoleService1 roleService=new RoleService1();
	 
	 @PreAuthorize("hasRole('GUEST') or hasRole('ADMIN')")
	 @GetMapping(value = "/roles1")
	 public ResponseEntity<List<Role>> getAllRoles() throws NoSuchRoleException {
	        List<Role> roleList = roleService.getAllRoles();
	        logger.info("get ALL ROLES");
	        //logger.info( System.getProperty("log4j.configurationFile"));
	        return new ResponseEntity<>(roleList, HttpStatus.OK);
	        
	    }
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @PostMapping("/roles1")
     public ResponseEntity<Role> addRole(@RequestBody Role role) {
			 Role nrole=roleService.createRole(role);
			 return new ResponseEntity<>(nrole, HttpStatus.CREATED);
		}
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @PutMapping(value = "/roles1/{id}")
	    public  ResponseEntity<Role> updateRole(@RequestBody Role uRole, @PathVariable Long id) throws NoSuchRoleException {
	        roleService.updateRole(uRole, id);
	        Role role = roleService.getRole(id);
	        return new ResponseEntity<>(role, HttpStatus.OK);
	    }
	 
	 @PreAuthorize("hasRole('ADMIN')")
	 @DeleteMapping("/roles1/{id}")
	 public ResponseEntity deleteRole(@PathVariable Long id) throws NoSuchRoleException, ExistsUsersForRoleException {
			roleService.deleteRole(id);
			return new ResponseEntity(HttpStatus.OK);
		}
	 
}