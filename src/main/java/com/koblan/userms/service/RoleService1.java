package com.koblan.userms.service;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
//import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import javax.transaction.Transactional;

import com.koblan.userms.dao.RoleDao;
import com.koblan.userms.exceptions.*;

@Service
public class RoleService1 {
	
	RoleDao roleDao=new RoleDao();
	
	public List<Role> getAllRoles() {
        return roleDao.getAll();
    }

    public Role getRole(Long id) throws NoSuchRoleException {
//      Role role = roleRepository.findOne(id);//1.5.9
        Role role = roleDao.get(id);
        if (role == null) throw new NoSuchRoleException();
        return role;
    }
    
    @Transactional
    public Role createRole(Role role) {
        return roleDao.save(role);
    }

    @Transactional
    public void updateRole(Role uRole, Long id) throws NoSuchRoleException {

        Role role = roleDao.get(id);
        if (role == null) throw new NoSuchRoleException();
        role.setRole(uRole.getRole());
        roleDao.update(role);
    }
    
    @Transactional
    public void deleteRole(Long id) throws NoSuchRoleException, ExistsUsersForRoleException {

    	Role role = roleDao.get(id);
        if (role == null) throw new NoSuchRoleException();
        if (role.getUsers().size() != 0) throw new ExistsUsersForRoleException();
        roleDao.delete(role);
    }
    
}
    