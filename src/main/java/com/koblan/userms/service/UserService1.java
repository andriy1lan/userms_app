package com.koblan.userms.service;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;

import com.koblan.userms.dao.RoleDao;
import com.koblan.userms.dao.UserDao;
import com.koblan.userms.exceptions.*;

@Service
public class UserService1 {
	
	RoleDao roleDao=new RoleDao();
	UserDao userDao=new UserDao();
	
	public List<User> getAllUsers() {
        return userDao.getAll();
        
    }
	
	public User getUser(Long id) throws NoSuchUserException {
        User user = userDao.get(id);
        if (user == null) throw new NoSuchUserException();
        return user;
    }
	
	@Transactional
    public User createUser(User user) {
		Role role=roleDao.findByNameIgnoreCase("Guest");
		Long id=3L; //ID of Guest
		//Role role=roleRepository.findById(id).get();
		Set<Role> r=new HashSet<Role>();
		r.add(role);
		user.setRoles(r);
		//user.getRoles().add(role);
		role.getUsers().add(user);
        return userDao.save(user);
    }
	
	@Transactional
    public User createUserWithAdminRole(User user) {
		Role role=roleDao.findByNameIgnoreCase("Guest");
		Role role2=roleDao.findByNameIgnoreCase("Admin");
		Long id=3L; //ID of Guest
		//Role role=roleRepository.findById(id).get();
		Set<Role> r=new HashSet<Role>();
		r.add(role);
		//user.getRoles().add(role);
		role.getUsers().add(user);
		
		Long id2=2L; //ID of Admin
		//Role role2=roleRepository.findById(id2).get();
		r.add(role2);
		role2.getUsers().add(user);
		user.setRoles(r);
        return userDao.save(user);
    }
	
	@Transactional
    public void updateUser(User nuser, Long id) throws NoSuchUserException {

        User user = userDao.get(id);
        
        if (user == null) throw new NoSuchUserException();
        user.setName(nuser.getName());
        user.setUsername(nuser.getUsername());
        user.setEmail(nuser.getEmail());
        user.setPassword(nuser.getPassword());
        user.setRoles(nuser.getRoles());
        userDao.update(user);  //merge
    }
	    
	    @Transactional
	    public void deleteUser(Long id) throws NoSuchUserException {
	    	User user = userDao.get(id);
	        if (user == null) throw new NoSuchUserException();
	        for(Role r:user.getRoles()) {r.getUsers().remove(user);}
	        userDao.delete(user);
	    }
	
}