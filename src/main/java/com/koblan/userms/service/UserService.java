package com.koblan.userms.service;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import com.koblan.userms.repository.RoleRepository;
import com.koblan.userms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.transaction.Transactional;
import com.koblan.userms.exceptions.*;

@Service
public class UserService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
    PasswordEncoder encoder;
	
	public UserRepository getUserRepository() {
		return userRepository;
	}
	
	public List<User> getAllUsers() {
        return userRepository.findAll();
        
    }
	
	public User getUser(Long id) throws NoSuchUserException {
        User user = userRepository.findById(id).get();//2.0.0.M7
        if (user == null) throw new NoSuchUserException();
        return user;
    }
	
	@Transactional
    public User createUser(User user) {
        Set<Role> roles=new HashSet<Role>();
        if (user.getRoles()!=null) {
        for(Role r:user.getRoles()) {Role role=roleRepository.findByRoleIgnoreCase(r.getRole()); 
        roles.add(role);};
        user.setRoles(roles); }
        String encpassword = user.getPassword();
        user.setPassword(encoder.encode(encpassword));
        //user.setRoles(nuser.getRoles());
        return userRepository.save(user);
    }
	
	@Transactional
    public User createUserWithAdminRole(User user) {
		Role role=roleRepository.findByRoleIgnoreCase("ROLE_GUEST");
		Role role2=roleRepository.findByRoleIgnoreCase("ROLE_ADMIN");
		Long id=3L; //ID of Guest
		//Role role=roleRepository.findById(id).get();
		Set<Role> r=new HashSet<Role>();
		r.add(role);
		//user.getRoles().add(role);
		role.getUsers().add(user);
		
		Long id2=2L; //ID of Admin
		//Role role2=roleRepository.findById(id2).get();
		r.add(role2);
		role2.getUsers().add(user);
		user.setRoles(r);
        return userRepository.save(user);
    }
	
	@Transactional
    public void updateUser(User nuser, Long id) throws NoSuchUserException {

        User user = userRepository.findById(id).get();
        
        if (user == null) throw new NoSuchUserException();
        user.setName(nuser.getName());
        user.setUsername(nuser.getUsername());
        user.setEmail(nuser.getEmail());
        if (!nuser.getPassword().equals("0"))
        user.setPassword(encoder.encode(nuser.getPassword()));
        Set<Role> updatedRoles=new HashSet<Role>();
        for(Role r:nuser.getRoles()) {Role role=roleRepository.findByRoleIgnoreCase(r.getRole()); 
        updatedRoles.add(role);};
        
        user.setRoles(updatedRoles);
        //user.setRoles(nuser.getRoles());
        userRepository.save(user);
    }
	
	    @Transactional
	    public void deleteUser(Long id) throws NoSuchUserException {
	    	User user = userRepository.findById(id).get();
	        if (user == null) throw new NoSuchUserException();
	        for(Role r:user.getRoles()) {r.getUsers().remove(user);}
	        userRepository.delete(user);
	    }
	
}
