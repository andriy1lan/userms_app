package com.koblan.userms.repository;

import com.koblan.userms.model.Role;
import com.koblan.userms.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	User findByUsernameIgnoreCase(String username);
	boolean existsByUsername(String username);
	boolean existsByEmail(String username);
}