package com.koblan.userms.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.koblan.userms.model.User;

import com.koblan.userms.session.SessionFactoryCreator;

@Component
public class UserDao implements Dao<User>{

	public User get (Long id) {
		return SessionFactoryCreator.getSession().get(User.class,id);
	}

	public List<User> getAll() {
		
		return (List<User>)SessionFactoryCreator.getSession().createQuery( "from User").list();
	}
	
	public User findByNameIgnoreCase (String name) {
		List<User> allusers =(List<User>)SessionFactoryCreator.getSession().createQuery( "from user").list();
		for (User u:allusers) {if (u.getUsername().equals(name)) return u;}
		return null;
	}

	public User save(User u) {
		Serializable id;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
		    tx = session.beginTransaction();
		    id=session.save(u);
		    
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		//    session.close();
		}
		   return session.get(User.class,id); 
	}

	public User update(User u) {
		User user;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
		    tx = session.beginTransaction();
		    user=(User)session.merge(u);
		    
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		   session.close();
		}
		  return user;
	}

	public void delete(User u) {
		User user;
		Session session=SessionFactoryCreator.getSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			user=(User)session.merge(u);
			session.delete(user);
		    tx.commit();
		}
		catch (HibernateException e) {
		    if (tx != null) tx.rollback();
		    throw e;
		}
		finally {
		   session.close();
		}
		
	}

}
