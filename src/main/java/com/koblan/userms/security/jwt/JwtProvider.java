package com.koblan.userms.security.jwt;

import io.jsonwebtoken.*;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.koblan.userms.credentials.UserPrinciple;

@Component
public class JwtProvider {
	
	private static final Logger logger = LogManager.getLogger();
	
	//@Value("${jwtSecret}")
    private String jwtSecret="123123";
 
    //@Value("${jwtExpiration}")
    private int jwtExpiration =3600000;
    
    public String generateJwtToken(Authentication authentication) {
    	 
        UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();
 
        return Jwts.builder()  //returns JWTBuiler
		                .setSubject((userPrincipal.getUsername()))  //Sets the JWT Claims sub (subject) value. -- returns JWT Builder
		                .setIssuedAt(new Date()) //Sets the JWT Claims iat (issued at) value.
		                .setExpiration(new Date((new Date()).getTime() + jwtExpiration)) //Sets the JWT Claims exp (expiration) value.
		                .signWith(SignatureAlgorithm.HS512, jwtSecret) //Signs the constructed JWT using the specified algorithm with the specified key (base64EncodedSecretKey), producing a JWS
		                .compact(); 
           //Actually builds the JWT and serializes it to a compact, URL-safe string according to the JWT Compact Serialization rules.
    }
 
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser()  //Returns a new JwtParser instance that can be configured and then used to parse JWT strings.
			                .setSigningKey(jwtSecret)  //Sets the signing key used to verify any discovered JWS digital signature.
			                .parseClaimsJws(token) // Parses the specified compact serialized JWS string based on the builder's current configuration state and returns the resulting Claims JWS instance.
			                .getBody().getSubject(); 
    }
    
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: ", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: ", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token: ", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token: ", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: ", e);
        }
        return false;
    }
 

}
