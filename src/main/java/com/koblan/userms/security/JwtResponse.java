package com.koblan.userms.security;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

public class JwtResponse {
	
	private String accessToken;
    private String tokenType = "Bearer";
    private String username;
	private Collection<? extends GrantedAuthority> authorities;
 
	public JwtResponse(String accessToken, String username, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.accessToken = accessToken;
		this.username = username;
		this.authorities = authorities;
	}
	
	
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	
	@Override
	public String toString() {
		return "JwtResponse [accessToken=" + accessToken + ", tokenType=" + tokenType + ", username=" + username
				+ ", authorities=" + authorities + "]";
	}
    

}
